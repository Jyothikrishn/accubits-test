import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { EmailInputComponent } from './components/form-controls/email-input/email-input.component';
import { PasswordInputComponent } from './components/form-controls/password-input/password-input.component';
import { AuthComponent } from './components/auth/auth.component';
import { AppRoutingModule } from './app.routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/home/intro/nav/nav.component';
import { IntroComponent } from './components/home/intro/intro.component';
import { KeyFeaturesComponent } from './components/home/key-features/key-features.component';
import { FeatureComponent } from './components/home/key-features/feature/feature.component';
import { AuthGuard } from './services/guards/auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    // FeatureComponent,
    // IntroComponent,
    // KeyFeaturesComponent,
    // NavComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
