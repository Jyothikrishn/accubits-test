import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from '../../components/home/home.component';
import { NavComponent } from '../../components/home/intro/nav/nav.component';
import { IntroComponent } from '../../components/home/intro/intro.component';
import { KeyFeaturesComponent } from '../../components/home/key-features/key-features.component';
import { FeatureComponent } from '../../components/home/key-features/feature/feature.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: HomeComponent
    }])
  ],
  declarations: [
    HomeComponent,
    NavComponent,
    IntroComponent,
    KeyFeaturesComponent,
    FeatureComponent
  ]
})
export class HomeModule { }
