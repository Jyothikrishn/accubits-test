import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthComponent } from '../../components/auth/auth.component';
import { EmailInputComponent } from '../../components/form-controls/email-input/email-input.component';
import { PasswordInputComponent } from '../../components/form-controls/password-input/password-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{
       path: '',
       component: AuthComponent
    }])
  ],
  declarations: [
    AuthComponent,
    EmailInputComponent,
    PasswordInputComponent
  ]
})
export class AuthModule { }
