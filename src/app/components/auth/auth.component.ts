import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup;
  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _router:Router
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.loginForm =  this._fb.group({})
  }

  doLogin(){
    console.log(this.loginForm)
    if(this.loginForm.valid){
      this._authService.setIsUSerLoggedIn(true);
      this._router.navigateByUrl("/home");
    }
  }

}
