import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-email-input',
  templateUrl: './email-input.component.html',
  styleUrls: ['./email-input.component.css']
})
export class EmailInputComponent implements OnInit {

  @Input() loginForm: FormGroup
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(){
    this.loginForm.addControl("email", new FormControl('', [Validators.required, Validators.email]))
  }

}
