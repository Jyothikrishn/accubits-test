import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.css']
})
export class PasswordInputComponent implements OnInit {

  @Input() loginForm: FormGroup
  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(){
    this.loginForm.addControl("password", new FormControl('', Validators.required))
  }

}
