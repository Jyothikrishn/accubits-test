import { Component, OnInit, Input, OnChanges } from '@angular/core';

export interface Feature{
  icon: string,
  hoverIcon: string,
  head: string,
  details: string
}

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit, OnChanges {

  @Input() feature: Feature;
  iconImg: string;
  constructor() { }

  ngOnChanges(){
    this.iconImg = this.feature.icon
  }

  ngOnInit() {
  }

  onHover(){
    this.iconImg = this.feature.hoverIcon
  }
  onLeave(){
    this.iconImg = this.feature.icon
  }

}
