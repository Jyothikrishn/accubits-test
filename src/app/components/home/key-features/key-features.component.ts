import { Component, OnInit } from '@angular/core';
import { Feature } from './feature/feature.component';

@Component({
  selector: 'app-key-features',
  templateUrl: './key-features.component.html',
  styleUrls: ['./key-features.component.scss']
})
export class KeyFeaturesComponent implements OnInit {
  desc = " Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s";
  keyFeatures: Array<Feature> = [
    {
      icon: "../../../../assets/img/icons/technical security.png",
      hoverIcon: "../../../../assets/img/icons/hover/technical security_hover.png",
      head: "Technical Security",
      details: this.desc
    },
    {
      icon: "../../../../assets/img/icons/data security.png",
      hoverIcon: "../../../../assets/img/icons/hover/data security_hover.png",
      head: "Data Security",
      details: this.desc
    },
    {
      icon: "../../../../assets/img/icons/identity theft.png",
      hoverIcon: "../../../../assets/img/icons/hover/identity theft_hover.png",
      head: "Identify Theft Prevention",
      details: this.desc
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
