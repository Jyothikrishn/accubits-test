import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {

  private isUSerLoggedIn$ = new BehaviorSubject<boolean>(false);
  constructor() { }

  setIsUSerLoggedIn(value: boolean){
    this.isUSerLoggedIn$.next(value);
  }

  get isUSerLoggedIn():Observable<boolean>{
    return this.isUSerLoggedIn$.asObservable()
  }

}
